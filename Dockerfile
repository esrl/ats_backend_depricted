FROM registry.esrl.dk:5005/atsv2/ats_janus

RUN apt update && apt install --no-install-recommends -y supervisor perl build-essential git ca-certificates libssl-dev netbase libpcap-dev libnet-dbus-perl cpanminus libidn2-0-dev libnet-dns-perl dbus avahi-daemon -y

# Fix limit of running one instance of avahi pr. box
RUN sed -i 's/^rlimit/#rlimit/' /etc/avahi/avahi-daemon.conf

# Install perl dependencies
RUN cpan install -T  Net::DBusinc::latest 
RUN cpan install -T  Canary::Stability 
RUN cpan install -T  Data::Dumper 
RUN cpan install -T  JSON 
RUN cpan install -T  Try::Tiny 
RUN cpan install -T  IO::Async::Stream
RUN cpan install -T  Net::Async::WebSocket
RUN cpan -T  IO::Async::Loop 
RUN cpan -T  IO::Async::Timer::Periodic 
RUN cpan -T -f  IO::Async::Loop::AnyEvent
RUN cpan -T -f  IO::Async::Loop::AnyEvent2::ElectricBoogaloo 

RUN cpan -T  AnyEvent::DBus 
RUN cpan -T  IPC::Run 
RUN cpan -T  Time::Piece 
RUN cpan -T  Time::Seconds 
RUN cpan -T  Filesys::Df  


# Install from archive
COPY Archive-Zip-1.64.tar.gz /
WORKDIR /
RUN tar xvfz Archive-Zip-1.64.tar.gz
WORKDIR /Archive-Zip-1.64
RUN ls -l 
RUN perl Makefile.PL
RUN make
RUN make install



RUN cpan -T -f  Search::Elasticsearch

# Install from archive
COPY Net-Pcap-0.18.tar.gz /
WORKDIR /
RUN tar xvfz Net-Pcap-0.18.tar.gz
WORKDIR /Net-Pcap-0.18
RUN perl Makefile.PL INC=-I/opt/pcap/include LIBS='-L/opt/pcap/lib -lpcap'
RUN make install


RUN cpanm --notest Net::PcapUtils
RUN cpan -T NetPacket::Ethernet

# packet only available from cpanm
RUN cpanm install NetPacket::IPv6 
RUN cpan -T NetPacket::UDP;

RUN cpan -T -f IO::Socket::Multicast6

COPY supervisord/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN mkdir -p /var/run/dbus/
#
## Get source code
#RUN git clone --single-branch --branch master --depth 1 https://gitlab.esrl.dk/CentreForBioRobotics/IntelligentBioRecording/ZooCam.git /ZooCam 
##RUN ln -s /mnt/manna/ZooCam /ZooCam
#
CMD /usr/bin/supervisord 
